const express = require("express");
const app = express();
const port = process.env.PORT || 5000;
const axios = require("axios");

// console.log that your server is up and running
app.listen(port, () => console.log(`Listening on port ${port}`));

// create a GET route
app.get("/express/backend", (req, res) => {
  res.send({ express: "YOUR EXPRESS BACKEND IS CONNECTED TO REACT" });
});

app.get("/programming", (req, res) => {
  console.log("programming");
  axios
    .get("http://quotes.stormconsultancy.co.uk/popular.json")

    .then(results => {
      console.log(results.data);
      res.send(results.data);
    })
    .catch(err => {});
});

app.get("/random", (req, res) => {
  console.log("random");
  axios
    .get("http://quotes.stormconsultancy.co.uk/random.json")
    .then(results => {
      console.log(results.data);
      res.send(results.data);
    })
    .catch(err => {});
});
